﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace Riat3
{
    public class ISerializerXML : ISerializer
    {
        public T Deserialize<T>(string obj)
        {
            XmlDocument xml = new XmlDocument();
            xml.LoadXml(obj);
            XmlSerializer seri = new XmlSerializer(typeof(T));
            using (var reader = new XmlNodeReader(xml))
            {
                return (T)seri.Deserialize(reader);
            }
        }
        public string Serialize<T>(T seriObject)
        {
            XmlSerializer seri = new XmlSerializer(typeof(T));
            XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
            ns.Add(string.Empty, string.Empty);
            StringBuilder outputString = new StringBuilder();
            var writer = XmlWriter.Create(outputString, new XmlWriterSettings() { OmitXmlDeclaration = true });
            seri.Serialize(writer, seriObject, ns);
            return outputString.ToString();
        }
    }

}
