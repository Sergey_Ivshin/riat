﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;

namespace Riat3
{
    class RequestMethodsHandler
    {
        private Input input;
        private ISerializerJson serializer;
        public HttpListenerContext context;

        public RequestMethodsHandler() {
            serializer = new ISerializerJson();
        }

        public void Ping()
        {
            context.Response.StatusCode =(int)HttpStatusCode.OK;
            context.Response.OutputStream.Close();
        }
        public void Stop()
        {
            context.Response.StatusCode = (int)HttpStatusCode.OK;
            context.Response.OutputStream.Close();
        }
        public void PostInputData() {
            byte[] buf= new byte[context.Request.ContentLength64];
            context.Request.InputStream.ReadAsync(buf, 0, Convert.ToInt32(context.Request.ContentLength64));
            string str = Encoding.UTF8.GetString(buf);
            input = serializer.Deserialize<Input>(str);
            context.Response.StatusCode = (int)HttpStatusCode.OK;
            context.Response.OutputStream.Close();
        }
        public void GetAnswer() {
            OutputBuilder builder = new OutputBuilder();
            Output output = builder.Build_Output(input);
            string out_string = serializer.Serialize<Output>(output);
            using (StreamWriter sw = new StreamWriter(context.Response.OutputStream)) {
                sw.WriteAsync(out_string);
            }
            context.Response.OutputStream.Close();
        }
        public void NotFound() {
            context.Response.StatusCode = (int)HttpStatusCode.BadRequest;
            context.Response.OutputStream.Close();
        }
    }
}
