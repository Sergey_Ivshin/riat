﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Threading.Tasks;

namespace Riat3
{
    class Program
    {
        static void Main(string[] args)
        {
            var host = "http://127.0.0.1";
            var port = Console.ReadLine();
            HttpServer server = new HttpServer();
            server.host = host;
            server.port = port;
            server.listener = new HttpListener();
            server.Initialize();
        }
    }
}
