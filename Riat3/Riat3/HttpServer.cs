﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Reflection;

namespace Riat3
{
    class HttpServer
    {
        public string host { set; get; }
        public string port { set; get; }
        public HttpListener listener { set; get; }
        HttpListenerContext listener_context;
        RequestMethodsHandler handler;
        private Thread serverThread;

        public void Initialize()
        {
            handler = new RequestMethodsHandler();
            serverThread = new Thread(Listener);
            serverThread.Start();
        }

        private void Listener()
        {
            
            string prefix = String.Format("{0}:{1}/", host, port);
            listener.Prefixes.Add(prefix);
            listener.Start();
            while (listener.IsListening) {
                listener_context = listener.GetContext();
                handler.context = listener_context;
                StartProcess();
                StopProcess();
            }
        }
        public void StopProcess() {
            string url = listener_context.Request.Url.AbsolutePath.Trim('/');
            url = url.Split('/').Last();
            if (url == "Stop") {
                Stop();
            }
        }
        private void Stop() {
            listener.Stop();
            serverThread.Abort();
        }
        public void StartProcess() {
            string url = listener_context.Request.Url.AbsolutePath.Trim('/');
            url = url.Split('/').Last();
            //todo: заменить на рефлексию, вызывать через handler.GetType().GetMethod()
            try {
                MethodInfo method = handler.GetType().GetMethod(url);
                method.Invoke(handler, null);
            }
            catch (Exception) {
                handler.NotFound();
            }
        }

        

    }
}
