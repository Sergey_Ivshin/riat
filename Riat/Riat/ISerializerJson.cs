﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization.Json;

namespace Riat
{
    public class ISerializerJson : ISerializer
    {
        public T Deserialize<T>(string obj)
        {
            MemoryStream stream = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(obj));
            DataContractJsonSerializer json = new DataContractJsonSerializer(typeof(T));
            stream.Position = 0;
            return (T)json.ReadObject(stream);
        }
        public string Serialize<T>(T seriObject)
        {
            try
            {
                decimal[] bacsCostili = (decimal[])seriObject.GetType().GetProperty("SortedInputs").GetValue(seriObject, new Object[0]);
                for (int i = 0; i < bacsCostili.Count(); i++)
                {
                    if (bacsCostili[i].ToString().Split(',').Count() == 1)
                    {
                        bacsCostili[i] = bacsCostili[i] / 10 * 10;
                    }
                }
                seriObject.GetType().GetProperty("SortedInputs").SetValue(seriObject, bacsCostili);
            }
            catch (Exception exp)
            {

            }

            return new System.Web.Script.Serialization.JavaScriptSerializer().Serialize(seriObject);
        }
    }
}
