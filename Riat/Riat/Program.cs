﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Web;

namespace Riat
{
    class Program
    {
        static void Main(string[] args)
        {
            var typeParam = Console.ReadLine();
            string obj = Console.ReadLine();
            Input input = new Input();
            ISerializer serializer;
            if (typeParam == "Xml")
            {
                serializer =new ISerializerXML();
            }
            else {
                serializer = new ISerializerJson();
            }
            input = serializer.Deserialize<Input>(obj);

            Output output = new Output();
            output.SumResult = input.Sums.Sum() * input.K;
            output.MulResult = 1;
            List<decimal> list = new List<decimal>(input.Sums);
            output.MulResult *= input.Muls.Aggregate((result,next)=>result*next);
            for (int i = 0; i < input.Muls.Length; i++)
            {

                list.Add(input.Muls[i]);
            }
            list.Sort();
            output.SortedInputs = list.ToArray();
            Console.WriteLine(serializer.Serialize<Output>(output));
            Console.ReadKey();
        }
    }
}
