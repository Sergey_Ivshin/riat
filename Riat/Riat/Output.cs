﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Riat
{
    [Serializable]
    [DataContract]
    public class Output
    {
        [DataMember(Name = "SumResult")]
        public decimal SumResult { get; set; }
        [DataMember(Name = "MulResult")]
        public int MulResult { get; set; }
        [DataMember(Name = "SortedInputs")]
        public decimal[] SortedInputs { get; set; }

        //todo2: структура данных это структура данных, не привыкайте писать бихнес логику в классе, который должен быть просто вместилищем даных
        //если ег опотребуется положить в бд, метод тоже будете класть?)
        public void setOutput(Input input)
        {
            SumResult = input.Sums.Sum() * input.K;
            MulResult = 1;
            List<decimal> list = new List<decimal>(input.Sums);
            for (int i = 0; i < input.Muls.Length; i++)
            {
                MulResult *= input.Muls[i];
                list.Add(input.Muls[i]);
            }
            list.Sort();
            SortedInputs = list.ToArray();
        }

    }
}
