﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace Riat2
{
    [Serializable]
    [DataContract]
    public class Input
    {
        [DataMember(Name = "K")]
        public int K { get; set; }
        [DataMember(Name = "Sums")]
        public decimal[] Sums { get; set; }
        [DataMember(Name = "Muls")]
        public int[] Muls { get; set; }
    }



}
