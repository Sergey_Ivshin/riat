﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riat2
{
    interface ISerializer
    {
        T Deserialize<T>(string obj);
        string Serialize<T>(T seriObject);
    }
}