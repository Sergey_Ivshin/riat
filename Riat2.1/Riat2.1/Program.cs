﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;

namespace Riat2
{
    class Program
    {
        static void Main(string[] args)
        {
            string url = "http://127.0.0.1";
            string port = Console.ReadLine();
            url += ':' + port;
            Client client = new Client();
            client.url = new Uri(url);
            while (!client.Ping()){ }
            Input input = client.Get<Input>("GetInputData");
            OutputBuilder build = new OutputBuilder();
            Output output = build.Build_Output(input);
            string req = client.Post("WriteAnswer", output);
        }
    }
}
