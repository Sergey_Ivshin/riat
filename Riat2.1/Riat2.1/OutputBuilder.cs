﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Riat2
{
    class OutputBuilder
    {
        public Output Build_Output(Input input) {
            Output output = new Output();
            output.SumResult = input.Sums.Sum() * input.K;
            output.MulResult = 1;
            List<decimal> list = new List<decimal>(input.Sums);
            output.MulResult *= input.Muls.Aggregate((result, next) => result * next);
            for (int i = 0; i < input.Muls.Length; i++)
            {
                list.Add(input.Muls[i]);
            }
            list.Sort();
            output.SortedInputs = list.ToArray();
            return output;
        }
    }
}
