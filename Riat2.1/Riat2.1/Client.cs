﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Net.Http;
using System.Threading.Tasks;

namespace Riat2
{
    class Client
    {
        public Uri url { get; set; }
        public bool Ping() {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = url;
            var response= httpClient.GetAsync("/Ping");
            return response.Result.IsSuccessStatusCode;
        }
        public T Get<T>(string method) {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = url;
            var response = httpClient.GetAsync(method);
            HttpContent content = response.Result.Content;
            string responseFromServer = content.ReadAsStringAsync().Result;
            ISerializer serializer = new ISerializerJson();
            T input = serializer.Deserialize<T>(responseFromServer);
            return input;
        }

        public string Post<T>(string method, T output)
        {
            HttpClient httpClient = new HttpClient();
            httpClient.BaseAddress = url;
            ISerializer serializer = new ISerializerJson();
            string outLine = serializer.Serialize<T>(output);
            byte[] bytes = Encoding.UTF8.GetBytes(outLine);
            HttpContent postbody =new ByteArrayContent(bytes);
            var response = httpClient.PostAsync(method,postbody);
            HttpContent content = response.Result.Content;
            string responseFromServer = content.ReadAsStringAsync().Result;
            return responseFromServer;
        }
    }
}
